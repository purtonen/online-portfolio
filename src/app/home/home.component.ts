import { Component } from '@angular/core'
import { ContentViewComponent } from '../content-view/content-view.component'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent extends ContentViewComponent {

  static title: string = ''

  constructor() {
    super()
    super.title = 'Eetu Purtonen - Software Developer'
  }

}
