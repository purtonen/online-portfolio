import { Component, Input, OnInit } from '@angular/core';
import { ResumeElement, ResumePart } from './resume-part.model';

@Component({
  selector: 'app-resume-part',
  templateUrl: './resume-part.component.html',
  styleUrls: ['./resume-part.component.scss']
})
export class ResumePartComponent implements OnInit {

  @Input() model: ResumePart

  constructor() { }

  ngOnInit(): void {
  }

}
