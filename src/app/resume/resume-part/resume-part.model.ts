export class ResumePart {
    name: string
    title: string
    elements: ResumeElement[]
}

export class ResumeElement {
    title: string
    subTitle: string
    meta: ResumeElementMeta
    content: ResumeElementContent
}

export class ResumeElementMeta {
    content: string
    url: string
}

export class ResumeElementContent {
    text: string
    list: ResumeElementContent[]
}