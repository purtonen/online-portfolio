import { Component } from '@angular/core'
import { ContentViewComponent } from '../content-view/content-view.component'
import { ResumePart } from './resume-part/resume-part.model'
import data from './resume.content.json'

@Component({
  selector: 'app-resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent extends ContentViewComponent {

  static title: string = 'Resume'

  resumeParts: ResumePart[] = data

  constructor() {
    super()
    super.title = ResumeComponent.title
    console.log(this.resumeParts)
  }

}
