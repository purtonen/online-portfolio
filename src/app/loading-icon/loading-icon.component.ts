import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-icon',
  template: `
    <div class="lds-ring">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
  `,
  styles: [
  ]
})
export class LoadingIconComponent {

  constructor() { }

}
