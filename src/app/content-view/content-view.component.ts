import { Component, OnInit } from '@angular/core';

@Component({
  template: 'NO UI HERE',
  styleUrls: ['./content-view.component.scss']
})
export class ContentViewComponent implements OnInit {

  title: string = ''

  constructor() {
  }

  public getTitle(): string {
    return this.title
  }

  ngOnInit(): void {
  }

}
