import { Component, OnInit } from '@angular/core';
import { ContentViewComponent } from '../content-view/content-view.component';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent extends ContentViewComponent {

  static title: string = '404 Not found'

  constructor() {
    super()
    super.title = PageNotFoundComponent.title
  }

}
