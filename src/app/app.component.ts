import { Component, HostBinding, HostListener, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  baseTitle: string = 'Eetu Purtonen'
  backgroundDotCount: number = 50
  arr: Array<number> = new Array(this.backgroundDotCount).fill(null)
  @HostBinding('class.scrolling') scrolling: boolean = false

  constructor(
    private router: Router,
    private titleService: Title,
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.router
      .events.pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          const child = this.activatedRoute.firstChild;
          if (child.snapshot.data['title']) {
            return child.snapshot.data['title'];
          }
          return '';
        })
      ).subscribe((title: string) => {
        if (!title) {
          this.titleService.setTitle(this.baseTitle);
        } else {
          this.titleService.setTitle(`${this.baseTitle} - ${title}`);
        }
      });
  }

  @HostListener('window:scroll') onScroll(): void {
    this.scrolling = window.scrollY > 0
  }
}
