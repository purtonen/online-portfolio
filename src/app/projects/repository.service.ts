import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JsonRepository, Repository } from './repository.model';

@Injectable()
export class RepositoryService {

    repos: Repository[] = []

    constructor(private httpClient: HttpClient) {
        this.httpClient.get("https://gitlab.com/api/v4/users/purtonen/projects")
            .subscribe((data: JsonRepository[]) => {
                data.forEach(el => {
                    this.repos.push({
                        name: el.name,
                        description: el.description,
                        url: el.web_url,
                        readmeUrl: el.readme_url,
                        tags: el.tag_list,
                        avatarUrl: el.avatar_url
                    })
                })
            });
    }

    getRepos(): Repository[] {
        return this.repos
    }

}