import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { ContentViewComponent } from '../content-view/content-view.component';
import { Repository } from './repository.model';
import { RepositoryService } from './repository.service';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})

export class ProjectsComponent extends ContentViewComponent {

  static title: string = 'Projects'
  repos: Repository[] = []

  constructor(private repoSvc: RepositoryService) {
    super()
    super.title = ProjectsComponent.title
  }

  ngOnInit(): void {
    super.ngOnInit()
    this.repos = this.repoSvc.getRepos()
  }

  getRepos(): Repository[] {
    return this.repos
  }

}
