export class Repository {
    name: string
    description: string
    url: string
    readmeUrl: string
    tags: string[]
    avatarUrl: string
}

export interface JsonRepository {
    name: string
    description: string
    web_url: string
    readme_url: string
    tag_list: string[]
    avatar_url: string
}