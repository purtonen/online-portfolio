import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProjectsComponent } from './projects/projects.component';
import { ResumeComponent } from './resume/resume.component';

const routes: Routes = [
  { path: 'contact', component: ContactComponent, data: { title: ContactComponent.title } },
  { path: 'projects', component: ProjectsComponent, data: { title: ProjectsComponent.title } },
  { path: 'resume', component: ResumeComponent, data: { title: ResumeComponent.title } },
  { path: '', component: HomeComponent, data: { title: HomeComponent.title } },
  { path: '**', component: PageNotFoundComponent, data: { title: PageNotFoundComponent.title } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
