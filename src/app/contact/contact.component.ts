import { Component, OnInit } from '@angular/core';
import { ContentViewComponent } from '../content-view/content-view.component';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent extends ContentViewComponent {

  static title: string = 'Contact'

  constructor() {
    super()
    super.title = ContactComponent.title
  }

}
