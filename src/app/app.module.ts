import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { DotComponent } from './dot/dot.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ContactComponent } from './contact/contact.component';
import { ProjectsComponent } from './projects/projects.component';
import { ResumeComponent } from './resume/resume.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { InlineSVGModule } from 'ng-inline-svg';
import { HttpClientModule } from '@angular/common/http';
import { ContentViewComponent } from './content-view/content-view.component';
import { RepositoryService } from './projects/repository.service';
import { ResumePartComponent } from './resume/resume-part/resume-part.component';
import { LoadingIconComponent } from './loading-icon/loading-icon.component';

@NgModule({
  declarations: [
    AppComponent,
    DotComponent,
    NavigationComponent,
    ContactComponent,
    ProjectsComponent,
    ResumeComponent,
    HomeComponent,
    PageNotFoundComponent,
    ContentViewComponent,
    ResumePartComponent,
    LoadingIconComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule,
    InlineSVGModule.forRoot()
  ],
  providers: [RepositoryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
