import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dot',
  templateUrl: './dot.component.html',
  styleUrls: ['./dot.component.scss']
})
export class DotComponent implements OnInit {

  radius: number = Math.random() * Math.floor(5)
  top: number = Math.random() * Math.floor(99)
  left: number = Math.random() * Math.floor(99)
  constructor() { }

  ngOnInit(): void {
  }

}
